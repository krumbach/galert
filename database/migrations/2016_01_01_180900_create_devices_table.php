<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDevicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->unsigned();
            $table->string('name');
            $table->string('api_key')->unique();;
            $table->dateTime('last_contact');
            $table->boolean('is_public')->default(false);
            $table->float('latitude')->default(0.0);
            $table->float('longitude')->default(0.0);

            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table( 'devices', function ( Blueprint $table )
        {
            $table->dropForeign( 'devices_account_id_foreign' );
        } );

        Schema::dropIfExists('devices');
    }
}
