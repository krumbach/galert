<?php

use App\Account;
use App\Device;
use App\User;
use Illuminate\Database\Seeder;

class DeviceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();

        foreach($users as $user)
        {
            $this->addAccountDevices($user->accounts()->first());
        }
    }

    private function addAccountDevices(Account $account)
    {
        for($i = 0; $i < 5; $i++)
        {
            $account->devices()->save(factory(Device::class)->make());
        }
    }
}
