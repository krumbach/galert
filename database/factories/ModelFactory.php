<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Http\TrendItemTypeOptions;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Account::class, function (Faker\Generator $faker) {
    return [
        'expiration' => $faker->dateTimeThisYear,
        'device_limit' => rand( 1, 30 ),
        'storage_limit' => 0
    ];
});

$factory->define(App\Device::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'api_key' => $faker->uuid,
        'last_contact' => $faker->dateTimeThisMonth
    ];
});

$factory->define(App\TrendItem::class, function (Faker\Generator $faker) {

    $units = ['mm', 'cm', 'C', 'F', 'oz'];

    return [
        'name' => $faker->name,
        'description' => $faker->sentence,
        'type' => array_rand( TrendItemTypeOptions::all() ),
        'unit' => array_rand( $units )
    ];
});

$factory->define(App\TrendValue::class, function (Faker\Generator $faker) {
    return [

    ];
});



