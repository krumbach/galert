<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrendItem extends Model
{
    public function device()
    {
        return $this->belongsTo(Device::class);
    }

    public function values()
    {
        return $this->hasMany(TrendValue::class, 'item_id', 'id' );
    }

    public function valuesBetweenDates( $start, $end )
    {
        return $this->values()->where('timestamp', '>=', $start)->where('timestamp', '<=', $end);
    }
}
