<?php

namespace App;


class TrendItemTypeOptions
{
    const Decimal = 'decimal';
    const String = 'string';

    public static function all()
    {
        return [ self::Decimal, self::String ];
    }
}