<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrendValue extends Model
{
    public function item()
    {
        return $this->belongsTo(TrendItem::class);
    }
}
