<?php

namespace App\Events;

use App\Events\Event;
use App\TrendValue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TrendValueWasAdded extends Event
{
    use SerializesModels;


    private $trendValue;

    public function __construct(TrendValue $trendValue)
    {
        $this->trendValue = $trendValue;
    }

}
