<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = [
        'name', 'timezone'
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function items()
    {
        return $this->hasMany(TrendItem::class);
    }

    public static function boot()
    {
        parent::boot();

        static::created( function ( $model )
        {
            $model->api_key = Device::generateKey();
            $model->save();
        } );
    }

    public static function generateKey()
    {
        $salt = sha1(time() . mt_rand());
        $newKey = substr($salt, 0, 40);

        return $newKey;
    }
}
