<?php


use App\Device;
use Carbon\Carbon;

function alert( $title = null, $message = null )
{
    $alert = app( 'App\Http\Alert' );

    if ( func_num_args() == 0 )
    {
        return $alert;
    }

    return $alert->info( $title, $message );
}

function growl( $title = null, $message = null )
{
    $growl = app( 'App\Http\Growl' );

    if ( func_num_args() == 0 )
    {
        return $growl;
    }

    return $growl->info( $title, $message );
}

function getDevicesSummaryModel( Device $device )
{
    $model = [];

    foreach($device->items as $item)
    {
        $i = [];
        $i['item'] = $item;
        $i['name'] = $item->name;
        $i['count'] = $item->values()->count();
        $i['last_value'] = $item->values()->orderBy('timestamp', 'desc')->first()->value;
        $i['last_timestamp'] = Carbon::parse($item->values()->orderBy('timestamp', 'desc')->first()->timestamp)->setTimezone( $device->timezone )->toRfc850String();
        $i['values'] = $item->values()->orderBy('timestamp', 'desc')->take(75)->lists('value')->reverse();

        $model[] = $i;
    }

    return $model;
}
