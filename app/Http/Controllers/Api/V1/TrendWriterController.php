<?php

namespace App\Http\Controllers\Api\v1;

use App\Events\TrendValueWasAdded;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TrendItem;
use App\TrendItemTypeOptions;
use App\TrendValue;
use Carbon\Carbon;

class TrendWriterController extends Controller
{
    public function postDecimal( Request $request )
    {
        $this->validate($request, [
            'name' => 'required|string',
            'value' => 'required|numeric',
            'timestamp' => 'required'
        ]);

        // Get the incoming data
        //
        $timestamp  = $request->input('timestamp');
        $name       = $request->input('name');
        $value      = $request->input('value');

        $value = floatval( $value );
        $type = TrendItemTypeOptions::Decimal;
        $device = $request->device;

        $data = $this->saveValue($device, $name, $type, $timestamp, $value);

        return response()->json($data);
    }

    private function saveValue($device, $name, $type, $timestamp, $value)
    {
        $trendItem = $device->items()->where( 'device_id', '=', $device->id)
        ->where('name', '=', $name)
        ->first();

        if( is_null( $trendItem ) )
        {
            // Create a new trending item.
            //
            $trendItem = new TrendItem();
            $trendItem->device_id    = $device->id;
            $trendItem->name         = $name;
            $trendItem->type         = $type;
            $trendItem->save();
        }

        // Add a new value for the item.
        //
        $trendValue = new TrendValue();
        $trendValue->item_id     = $trendItem->id;
        $trendValue->timestamp   = Carbon::parse($timestamp); //date('Y-m-d H:i:s', $timestamp);
        $trendValue->value       = $value;
        $trendValue->save();

        // Update the last contact date.
        //
        $device->last_contact = Carbon::now();
        $device->save();

        $result = [
            'timestamp'     => $trendValue->timestamp,
            'value'         => $value,
            'type'          => $trendItem->type,
            'item_id'       => $trendItem->id,
            'item_name'     => $trendItem->name,
            'value_id'      => $trendValue->id
        ];

        event(new TrendValueWasAdded($trendValue));

        return $result;
    }
}
