<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $devices = Auth::user()->devices;

        $models = [];

        foreach($devices as $device)
        {
            $model = [];
            $model['device'] = $device;
            $model['items'] = getDevicesSummaryModel( $device );

            $models[$device->name] = $model;
        }

        return view('home', [ 'devices' => $devices, 'models' => $models ]);
    }
}
