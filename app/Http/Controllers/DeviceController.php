<?php

namespace App\Http\Controllers;

use App\Account;
use App\Device;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class DeviceController extends Controller
{

    public function index()
    {
        //
    }


    public function create(Request $request, $id)
    {
        $user = Auth::user();
        $account = $user->accounts()->findOrfail($id);
        $timezones = $this->timeZoneList();

        return view('devices.create', ['account' => $account, 'timezones' => $timezones]);
    }


    public function store(Request $request, $id)
    {
        $user = Auth::user();
        $account = $user->accounts()->findOrfail($id);

        $name = $request->input('name');
        $tz = $request->input('timezone');

        $account->devices()->create([
            'name' => $name,
            'timezone' => $tz,
            'last_contact' => Carbon::now()
        ]);

        return redirect()->route('account.show', [$account]);
    }

    public function show(Account $account, Device $device)
    {
        $items = [];

        $timezone = $device->timezone;

        $start = Carbon::now( $timezone )->subDays(7)->startOfDay()->setTimezone( 'UTC' );
        $end = Carbon::now( $timezone )->endOfDay()->setTimezone( 'UTC' );

        //$start = Carbon::now()->subDays(5)->startOfDay();
       // $start = Carbon::now()->startOfDay();

        foreach($device->items as $item)
        {
            $values = $item->valuesBetweenDates( $start, $end )->lists('value', 'timestamp');

            $points = [];
            foreach($values as $datetime => $value)
            {
                $data = [Carbon::parse($datetime )->setTimezone( $timezone )->toIso8601String(), $value];
                $points[] = $data;
            }

            $i = [];
            $i['item'] = $item;
            $i['points'] = $points;

            $items[] =$i;
        }

        return view('devices.show', [ 'account' => $account, 'device' => $device, 'items' => $items ]);
    }

    private function timeZoneList()
    {
        $zones_array = [];
        $timestamp   = time();
        foreach ( timezone_identifiers_list() as $key => $zone )
        {
            date_default_timezone_set( $zone );
            $zones_array[ $key ][ 'zone' ]          = $zone;
            $zones_array[ $key ][ 'diff_from_GMT' ] = 'UTC/GMT ' . date( 'P', $timestamp );
        }

        return $zones_array;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
