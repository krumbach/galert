<?php

namespace App\Http\Controllers;

use App\Account;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function create()
    {
        return view( 'accounts.create' );
    }

    public function store(Request $request)
    {
        $user = Auth::user();

        $packages = Config::get('galert.packages');
        $packages = json_decode(json_encode($packages), FALSE);

        $name = $request->input('package');
        $package = $packages->$name;

        $user->accounts()->create([
            'name' => $package->name,
            'expiration' => Carbon::now()->addDays($package->expiration),
            'device_limit' => $package->max_devices,
            'storage_limit' => 0
        ]);

        growl()->success('Account added.');

        return redirect()->route('profile.index');
    }


    public function show(Request $request, $id)
    {
        $user = Auth::user();

        $account = $user->accounts()->findOrfail($id);

        return view( 'accounts.show', [ 'account' => $account ] );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
