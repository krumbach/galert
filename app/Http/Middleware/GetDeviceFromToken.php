<?php

namespace App\Http\Middleware;

use App\Device;
use Closure;

class GetDeviceFromToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get from headers.
        //
        $key = $request->header('X-Authorization');

        // Try getting the key from elsewhere.
        //
        if (empty($key))
        {
            $key = $request->input('X-Authorization');
        }

        // It's still empty.
        //
        if (empty($key))
        {
            if ($request->ajax())
            {
                return response('Unauthorized.', 401);
            }
            else
            {
                return redirect()->guest('login');
            }
        }

        $device = Device::whereApiKey($key)->first();

        if( is_null( $device ) )
        {
            return response('Unauthorized.', 401);
        }

        $request->device = $device;

        return $next($request);
    }
}
