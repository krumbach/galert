<?php

namespace App\Http;


class Alert
{
    private $config = [
        'showConfirmButton' => false,
        'timer'             => 2100,
        'allowOutsideClick' => true
    ];

    public function create($title, $text, $type)
    {
        $this->config['title'] = $title;
        $this->config['text'] = $text;
        $this->config['type'] = $type;

        $this->flashConfig();

        return $this;
    }

    public function info($text, $title = 'Info')
    {
        $this->create($title, $text, 'info');

        return $this;
    }

    public function success($text, $title = 'Success')
    {
        $this->create($title, $text, 'success');

        return $this;
    }

    public function warning($text, $title = 'Warning')
    {
        $this->create($title, $text, 'warning');

        return $this;
    }

    public function error($text, $title = 'Error')
    {
        $this->create($title, $text, 'error');

        return $this;
    }

    public function autoclose($milliseconds = 1800)
    {
        $this->config['timer'] = $milliseconds;

        $this->flashConfig();

        return $this;
    }

    public function confirmButton($buttonText = 'OK')
    {
        $this->config['confirmButtonText'] = $buttonText;
        $this->config['showConfirmButton'] = true;

        $this->flashConfig();

        return $this;
    }

    public function persistent($buttonText = 'OK')
    {
        $this->config['confirmButtonText'] = $buttonText;
        $this->config['showConfirmButton'] = true;
        $this->config['allowOutsideClick'] = false;
        $this->config['timer'] = 'null';

        $this->flashConfig();

        return $this;
    }

    public function persistent2()
    {
        $this->config['showConfirmButton'] = false;
        $this->config['allowOutsideClick'] = false;
        $this->config['timer'] = 'null';

        $this->flashConfig();

        return $this;
    }

    private function flashConfig()
    {
        session()->flash("sweet_alert", $this->buildConfig());
    }

    private function buildConfig()
    {
        return json_encode($this->config);
    }
}
