<?php

namespace App\Http;


class Growl
{
    private $config = [
        'position' => 'bottom-right',
        'sticky'   => false,
        'time'     => 4000
    ];

    public function create($title, $text, $type)
    {
        $this->config['title'] = $title;
        $this->config['text'] = $text;
        $this->config['class_name'] = $type;

        $this->flashConfig();

        return $this;
    }

    public function info($text, $title = 'Info')
    {
        $this->create($title, $text, 'clean');

        return $this;
    }

    public function success($text, $title = 'Success')
    {
        $this->create($title, $text, 'success');

        return $this;
    }

    public function warning($text, $title = 'Warning')
    {
        $this->config['position'] = 'top-right';

        $this->create($title, $text, 'warning');

        return $this;
    }

    public function error($text, $title = "Error")
    {
        $this->config['position'] = 'top-right';

        $this->create($title, $text, 'danger');

        return $this;
    }

    public function topRight()
    {
        $this->config['position'] = 'top-right';

        $this->flashConfig();

        return $this;
    }

    public function bottomRight()
    {
        $this->config['position'] = 'bottom-right';

        $this->flashConfig();

        return $this;
    }

    public function topLeft()
    {
        $this->config['position'] = 'top-left';

        $this->flashConfig();

        return $this;
    }

    public function bottomLeft()
    {
        $this->config['position'] = 'bottom-left';

        $this->flashConfig();

        return $this;
    }

    public function time($milliseconds = 4000)
    {
        $this->config['time'] = $milliseconds;

        $this->flashConfig();

        return $this;
    }

    private function flashConfig()
    {
        session()->flash("sweet_growl", $this->buildConfig());
    }

    private function buildConfig()
    {
        return json_encode($this->config);
    }
}
