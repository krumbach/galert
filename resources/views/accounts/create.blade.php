@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Account</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ route('account.store') }}">
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="hidden" name="package" value="free">
                                                <button type="submit" class="btn btn-primary">
                                                    Free
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-4">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ route('account.store') }}">
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="hidden" name="package" value="silver">
                                                <button type="submit" class="btn btn-primary">
                                                    Silver
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-md-4">
                                    <form class="form-horizontal" role="form" method="POST" action="{{ route('account.store') }}">
                                        {!! csrf_field() !!}
                                        <div class="form-group">
                                            <div class="col-md-8 col-md-offset-2">
                                                <input type="hidden" name="package" value="gold">
                                                <button type="submit" class="btn btn-primary">
                                                    Gold
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
