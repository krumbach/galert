@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Account {{ $account->name }} Devices
                        <a class="btn btn-xs btn-primary pull-right" href="{{ route('account.device.create', [$account]) }}">Add</a>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            @forelse($account->devices as $device)
                                <li>
                                    <a href="{{ route('account.device.show', [$account, $device]) }}">{{ $device->name }}</a> <small>({{$device->timezone}})</small>
                                </li>
                                api-key: <code>{{ $device->api_key }}</code>
                            @empty
                                <li>no devices</li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
