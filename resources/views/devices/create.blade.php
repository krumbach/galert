@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Device</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form class="form-horizontal" role="form" method="POST" action="{{ route('account.device.store', [$account]) }}">
                                    {!! csrf_field() !!}
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                <label class="col-md-2 control-label">Name</label>

                                                <div class="col-md-8">
                                                    <input type="text" class="form-control" required="required" name="name" value="{{ old('name') }}">

                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                                <label class="col-md-2 control-label">Timezone</label>

                                                <div class="col-md-8">
                                                    <select name="timezone" class="form-control">
                                                        <?php
                                                        foreach($timezones as $tz)
                                                        {
                                                            $selected = old('time_zone', 'America/Chicago') == $tz['zone'] ? 'selected="selected"' : '';
                                                            echo '<option value="' . $tz['zone'] . '" ' . $selected . '>' . $tz['zone'] . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-primary">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
