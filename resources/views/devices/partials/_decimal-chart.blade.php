<h3>{{ $trend_item['item']->name }} ({{ $trend_item['item']->values()->count() }})</h3>
<div id="{{ $trend_item['item']->id }}" class="line-chart {{ $trend_item['item']->type }}" data-points="{{ json_encode($trend_item['points']) }}"></div>
