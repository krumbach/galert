@extends('layouts.app')

@section('content')

    <div class="container">
        <h2 class="page-header">Device: {{ $device->name }} ({{ $device->items()->count() }})</h2>
        <div class="row">
            @foreach($items as $trend_item)
                <div class="col-md-6">
                    @include('devices.partials._decimal-chart')
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <script>

        (function ($) {

            $(document).ready(function () {
                google.charts.load('current', {packages: ['corechart', 'line']});
                google.charts.setOnLoadCallback(drawBasic);
            });

            function drawBasic() {

                $('.line-chart').each(function(){
                    var that = this;

                    var points = $(that).data('points');

                    for(var i = 0; i < points.length; i++) {
                        points[i][0] = new Date(points[i][0]);
                        points[i][1] = parseFloat(points[i][1]);
                    }

                   console.log(points);

                    var data = new google.visualization.DataTable();
                    data.addColumn('datetime', 'X');
                    data.addColumn('number', 'Value');

                    data.addRows(points);

                    var options = {
                        width: '100%',
                        height: 500,
                        legend: {position: 'none'},
                        enableInteractivity: false,
                        chartArea: {
                            width: '85%'
                        },
                        hAxis: {
                            gridlines: {
                                count: -1,
                                units: {
                                    days: {format: ['MMM dd']},
                                    hours: {format: ['HH:mm', 'ha']},
                                }
                            },
                            minorGridlines: {
                                units: {
                                    hours: {format: ['hh:mm:ss a', 'ha']},
                                    minutes: {format: ['HH:mm a Z', ':mm']}
                                }
                            }
                        }
                    };


                    var chart = new google.visualization.LineChart(that);

                    chart.draw(data, options);
                });

            }

        })(jQuery);
    </script>

@endsection
