@extends('layouts.app')

@section('styles')

@endsection

@section('content')
<div class="container spark-screen">
    <div class="row">
        @foreach($models as $key => $model)
            <div class="col-md-6">
                <h3><a href="{{ route('account.device.show', [$model['device']->account, $model['device']])}}">{{ $key }}</a></h3>
                <table class="table">
                    <tbody>
                    @foreach($model['items'] as $item)
                        <tr>
                            <td class="item-name"><h4>{{ $item['name'] }}</h4></td>
                            <td>
                                <p class="item-value">{{ $item['last_value'] }}</p>
                                <p class="item-time">{{ $item['last_timestamp'] }}</p>
                            </td>
                            <td><span class="inlinesparkline"> {{ implode(",", $item['values']->toArray()) }} </span> </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endforeach
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript" src="/libs/jquery.sparkline/js/jquery.sparkline.js"></script>
    <script type="text/javascript">

        (function ($) {

            $(document).ready(function () {
                $('.inlinesparkline').sparkline('html',
                        {
                            type: 'line',
                            width: '150',
                            height: '45'
                        } );
            });

        })(jQuery);
    </script>
@endsection
