@if (session()->has('sweet_alert'))
    <script>
        GALERT.alert( {!! session('sweet_alert') !!} );
    </script>
@endif