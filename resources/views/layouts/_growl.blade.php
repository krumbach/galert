@if (session()->has('sweet_growl'))
    <script>
        GALERT.growl( {!! session('sweet_growl') !!} );
    </script>
@endif