@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Accounts
                        <a class="btn btn-xs btn-primary pull-right" href="{{ route('account.create') }}">Add</a>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            @forelse($accounts as $account)
                                <li>
                                    <a href="{{ route('account.show', [$account]) }}">
                                        {{ $account->name }}
                                    </a>: expires
                                    (<small>{{ $account->expiration }}</small>)
                                </li>
                            @empty
                                <li>no accounts</li>
                            @endforelse
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
