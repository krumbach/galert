<?php

return [

    'packages' => [
        'free' => [
            'name' => 'free',
            'max_devices' => 1,
            'max_items_per_device' => 2,
            'expiration' => 120
        ],
        'silver' => [
            'name' => 'silver',
            'max_devices' => 5,
            'max_items_per_device' => 5 ,
            'expiration' => 356
        ],
        'gold' => [
            'name' => 'gold',
            'max_devices' => 25,
            'max_items_per_device' => 25,
            'expiration' => 356
        ]
    ]

];
