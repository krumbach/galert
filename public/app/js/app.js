var GALERT = GALERT || {};

GALERT.growl = function(options) {
    $.gritter.add(options);
};

GALERT.alert = function(options) {
    swal(options);
};